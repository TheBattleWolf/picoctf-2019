#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main()
{
	char challenge[]="ujgkkafylzwjmtaugfjzmvqrft";

	for(int j=0; j<26; j++)
	{
		for(int i=0; i<strlen(challenge); i++)
		{
			if(isupper(challenge[i]))
			{
				if(challenge[i]+j>0x5A)
				{
					printf("%c", challenge[i]+j-0x5A+0x40);
				}
				else
				{
					printf("%c", challenge[i]+j);
				}
			}
			else if(islower(challenge[i]))
			{
				if(challenge[i]+j>0x7A)
				{
					printf("%c", challenge[i]+j-0x7A+0x60);
				}
				else
				{
					printf("%c", challenge[i]+j);
				}
			}
			else
			{
				printf("%c", challenge[i]);
			}
		}
		
		printf("\n");
	}


	return 0;
}
