#include <stdio.h>
#include <stdlib.h>

#define FILELENGTH 2976

int main()
{
	FILE *whitepages=fopen("./whitepages.txt", "r");
	unsigned char c;

	if(whitepages!=NULL)
	{
		for(int k=20; k<FILELENGTH/5; k++)
		{
			for(int i=0; (c=fgetc(whitepages))!=(unsigned char)EOF; i++)
			{
				switch(c)
				{
					case 0x20:
						putchar(c);
						break;
					case 0x80:
						putchar('O');
						break;
					case 0x83:
						putchar('X');
						break;
					case 0xe2:
						putchar('I');
						break;
					default:
						printf("%x", c);
						break;
				}
				if(i%k==0)
					putchar('\n');
			}
			putchar('\n');

			rewind(whitepages);
		}

		fclose(whitepages);
	}

	return 0;
}
