#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define ROT 13

int main()
{
	char challenge[]="cvpbPGS{abg_gbb_onq_bs_n_ceboyrz}";

	for(int i=0; i<strlen(challenge); i++)
	{
		if(isupper(challenge[i]))
		{
			if(challenge[i]+ROT>0x5A)
			{
				printf("%c", challenge[i]+ROT-0x5A+0x40);
			}
			else
			{
				printf("%c", challenge[i]+ROT);
			}
		}
		else if(islower(challenge[i]))
		{
			if(challenge[i]+ROT>0x7A)
			{
				printf("%c", challenge[i]+ROT-0x7A+0x60);
			}
			else
			{
				printf("%c", challenge[i]+ROT);
			}
		}
		else
		{
			printf("%c", challenge[i]);
		}
	}

	printf("\n");

	return 0;
}
